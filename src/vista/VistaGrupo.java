package vista;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import modelo.Grupo;

/**
 * Fichero: VistaGrupo.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 30/11/2015
 */

public class VistaGrupo implements IVista<Grupo> {
    private String id;
    private String nombre;

    public Grupo tomaDatos() throws InputMismatchException {
    /*
    * Toma los datos proporcionados por teclado
    * y los almacena en una variable de tipo Grupo.
    */
    Grupo grupo = new Grupo();
    
    System.out.println("\nINTRODUCIR DATOS");
    System.out.println("ID: ");
    id = Vista.leerTexto();
    System.out.println("Nombre: ");
    nombre = Vista.leerTexto();
    
    grupo.setId(id);
    grupo.setNombre(nombre);
    
    return grupo;
  }

  public void muestraDatos(Grupo grupo) {
    /*
    * Muestra los datos del grupo
    */
      System.out.println("* ---------------------------------------");
      System.out.println("* DATOS DEL GRUPO");
      System.out.println("* ID: " + grupo.getId() + "      ||     Nombre: " + grupo.getNombre());
  }
  
  public void mostrarVarios(HashSet hashset) {
        IVista<Grupo> vg = new VistaGrupo();
        Iterator iterator = hashset.iterator();
        Grupo grupo = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            grupo = (Grupo) iterator.next();
            vg.muestraDatos(grupo);
            System.out.println("********************************************************************************");
        }
    }
}
