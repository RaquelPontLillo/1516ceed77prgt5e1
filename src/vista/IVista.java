package vista;

/**
 * Fichero: IVista.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 30/11/2015
 */

public interface IVista<T> {
    public void muestraDatos(T t);
    public T tomaDatos();
}
