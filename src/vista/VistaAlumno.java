package vista;

import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Iterator;
import modelo.Alumno;
import modelo.Grupo;

/**
 * Fichero: VistaAlumno.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 30/11/2015
 */

public class VistaAlumno implements IVista<Alumno> {
  private String id;
  private String nombre;
  private int edad;
  private String email;
  private Grupo grupo;

  public Alumno tomaDatos() throws InputMismatchException {
    /*
    * Toma los datos proporcionados por teclado
    * y los almacena en una variable de tipo Alumno.
    */
    Alumno alumno = new Alumno();
    Boolean valido;
    Boolean error = true;
    
    System.out.println("\nINTRODUCIR DATOS");
    System.out.println("ID: ");
    id = Vista.leerTexto();
    System.out.println("Nombre: ");
    nombre = Vista.leerTexto();
    
    do {
        System.out.println("Introduce el e-mail del alumno: ");
            email = Vista.leerTexto();
            valido = Vista.validarEmail(email);
            if (valido) {
                error = false;
            } else {
                System.err.println("\nEl e-mail introducido no es válido.");
            }
        } while (error);
        
        error = true;

        do {
            try {
                System.out.println("Introduce la edad del alumno: ");
                edad = Vista.leerNumero();
                error = false;
            } catch (InputMismatchException e) {
                System.err.println("\nLa edad debe ser numérica.");
          }
        } while (error);
    
    
    alumno.setId(id);
    alumno.setNombre(nombre);
    alumno.setEdad(edad);
    alumno.setEmail(email);
    
    return alumno;
  }

  public void muestraDatos(Alumno alumno) {
    /*
    * Muestra los datos del alumno
    */
      System.out.println("\n*****************************************");
      System.out.println("* MOSTRANDO DATOS DEL ALUMNO");
      System.out.println("* ID: " + alumno.getId() + "   ||  Nombre: " + alumno.getNombre());
      System.out.println("* Edad: " + alumno.getEdad() + "   ||  E-mail: " + alumno.getEmail());
      VistaGrupo vc = new VistaGrupo();
      vc.muestraDatos(alumno.getGrupo());
      System.out.println("*****************************************");
  }
  
    public void mostrarVarios(HashSet hashset) {
        IVista<Alumno> va = new VistaAlumno();
        Iterator iterator = hashset.iterator();
        Alumno alumno = null;

        while (iterator.hasNext()) {
            System.out.println("\n********************************************************************************");
            alumno = (Alumno) iterator.next();
            va.muestraDatos(alumno);
            System.out.println("********************************************************************************");
        }
    }
}
