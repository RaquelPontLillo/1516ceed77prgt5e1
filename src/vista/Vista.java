package vista;

import controlador.Configurador;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Fichero: Vista.java
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/01/2016
 */

public class Vista {
    public static String leerTexto() {
        Scanner sc = new Scanner(System.in);
        String texto = sc.nextLine();
        return texto;
    }
    
    public static int leerNumero() {
        Scanner scanner = new Scanner(System.in); 
        int numero = scanner.nextInt();
        return numero;
    }
        
    public static char leerCaracter() {
        Scanner scanner = new Scanner(System.in);
        String cadena = scanner.next().toLowerCase();
        char caracter = cadena.charAt(0);
        return caracter;
    }
        
    public static void error() {
        System.err.println("El valor introducido no es numérico. Por favor, introduce un número.");
    }
    
    public static void mostrarTexto(String texto) {
        System.out.println(texto);
    }
    
    public static void mostrarTextoError(String texto) {
        System.err.println(texto);
    }
    
    public static boolean validarEmail(String s) {
        Pattern pattern = Pattern.compile("([\\w\\.]+)([@])([\\w+])([\\.])([\\w+])");
        Matcher matcher = pattern.matcher(s);
        return matcher.matches();
    }
    
    public char menuEstructura() {
        String nombre = Configurador.getNombreApp();
        char opt;
        
        System.out.println("\n" + nombre);
        System.out.println("\n********************************");
        System.out.println("* ESCOGE EL TIPO DE ESTRUCTURA");
        System.out.println("* ------------------------------");
        System.out.println("* s. Salir");
        System.out.println("* ------------------------------");
        System.out.println("* a. Array");
        System.out.println("* h. HashSet");
        System.out.println("********************************");
        System.out.println("\nEscoge una opción: ");
        opt = leerCaracter();
        return opt;
    }
    
    public int menu() {
        int opcion;
        String nombre = Configurador.getNombreApp();
        
        System.out.println("\n" + nombre);
        System.out.println("\n***************************");
        System.out.println("* MENÚ PRINCIPAL");
        System.out.println("* -------------------------");
        System.out.println("* 0. Salir");
        System.out.println("* -------------------------");
        System.out.println("* 1. Grupo");
        System.out.println("* 2. Alumno");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opcion = leerNumero();
        
        return opcion;
    }
    
    public static char menuCrud() {
        char opt;
        System.out.println("\n***************************");
        System.out.println("* MENÚ C.R.U.D.");
        System.out.println("* -------------------------");
        System.out.println("* e. Salir (exit)");
        System.out.println("* -------------------------");
        System.out.println("* c. Crear (create)");
        System.out.println("* r. Leer (read)");
        System.out.println("* u. Actualizar (update)");
        System.out.println("* d. Borrar (delete)");
        System.out.println("***************************");
        System.out.println("\nEscoge una opción: ");
        opt =  leerCaracter();
        return opt;
    }
}
