package modelo;

/**
 * Fichero: Grupo.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 30/11/2015
 */

public class Grupo {
    /*
    * La clase grupo proporciona los constructores
    * para crear objetos de tipo Grupo y los métodos
    * para recuperar el valor de los atributos de esos 
    * objetos o para darles un nuevo valor.
    */
    
    //Variables de la clase
    private String id;
    private String nombre;
    
    //Constructores de la clase
    public Grupo () {
        
    }
    
    public Grupo(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    //Getters
    public String getId() {
        return id;
    }
    
    public String getNombre() {
        return nombre;
    }

    //Setters
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setId(String id) {
        this.id = id;
    }
}
