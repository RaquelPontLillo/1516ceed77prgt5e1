package modelo;

import java.util.HashSet;

/**
 * Fichero: Controlador.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class ModeloVector implements IModelo {
    private Alumno alumnos[] = new Alumno[99];
    private Grupo grupos[] = new Grupo[99];
    int idA = 0;
    int idG = 0;
    
    public ModeloVector() {
        for (int i = 0; i < alumnos.length; i++) {
            alumnos[i] = new Alumno("","",0,"",null);
        }
        
        for (int i = 0; i < grupos.length; i++) {
            grupos[i] = new Grupo("","");
        }   
    }
    
    @Override
    public void create(Alumno alumno) {
        alumnos[idA] = alumno;
        idA++;
    }
    
    @Override
    public void create(Grupo grupo) {
        grupos[idG] = grupo;
        idG++;
    }
    
    @Override
    public HashSet<Alumno> readAlumno() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < alumnos.length; i++) {
            if ( !alumnos[i].getId().equals("")) {
                hashset.add(alumnos[i]);
            }
        }
        return hashset;
    }
    
    @Override
    public HashSet<Grupo> readGrupo() {
        HashSet hashset = new HashSet();
        for (int i = 0; i < grupos.length; i++) {
            if ( !grupos[i].getId().equals("")) {
                hashset.add(grupos[i]);
            }
        }
        return hashset;
    }
    
    
    @Override
    public void update(Alumno alumno) {
        for (int i = 0; i < alumnos.length; i++) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = alumno;
            }
        }
    }
    
    @Override
    public void update(Grupo grupo) {
        for (int i = 0; i < grupos.length; i++) {
            if (grupos[i].getId().equals(grupo.getId())) {
                grupos[i] = grupo;
            }
        }
    }

    
    @Override
    public void delete(Alumno alumno) {
        for (int i = 0; i < alumnos.length; i++) {
            if (alumnos[i].getId().equals(alumno.getId())) {
                alumnos[i] = new Alumno("","",0,"",null);
            }
        }
    }
    
    @Override
    public void delete(Grupo grupo) {
        for (int i = 0; i < grupos.length; i++) {
            if (grupos[i].getId().equals(grupo.getId())) {
                grupos[i] = new Grupo("","");
            }
        }
    }
}