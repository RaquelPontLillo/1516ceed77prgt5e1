package modelo;

import java.util.HashSet;

/**
 * Fichero: Controlador.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class ModeloHashSet implements IModelo {
    private HashSet<Alumno> alumnos = new HashSet();
    private HashSet <Grupo> grupos = new HashSet(); 

    @Override
    public void create(Alumno alumno) {
        alumnos.add(alumno);
    }
    
    @Override
    public void create(Grupo grupo) {
        grupos.add(grupo);
    }

    @Override
    public HashSet readAlumno() {
        return alumnos;
    }

    @Override
    public HashSet readGrupo() {
        return grupos;
    }
    
    @Override
    public void update(Alumno alumno) {
        Alumno toRemove = null;
        for (Alumno a: alumnos) {
            if(a.getId().equals(alumno.getId())) {
                toRemove = a;
            }
        }
        alumnos.remove(toRemove);
        alumnos.add(alumno);
    }
    
    @Override
    public void update(Grupo grupo) {
        Grupo toRemove = null;
        for (Grupo g: grupos) {
            if(g.getId().equals(grupo.getId())) {
                toRemove = g;
            }
        }
        grupos.remove(toRemove);
        grupos.add(grupo);
    }

    @Override
    public void delete(Alumno alumno) {
        Alumno toRemove = null;
        for (Alumno a: alumnos) {
            if (a.getId().equals(alumno.getId())) {
                toRemove = a; 
            }
        }
        alumnos.remove(toRemove);
    }
    
    @Override
    public void delete(Grupo grupo) {
        Grupo toRemove = null;
        for (Grupo g: grupos) {
            if (g.getId().equals(grupo.getId())) {
                toRemove = g;
            }
        }
        grupos.remove(toRemove);
    }
}