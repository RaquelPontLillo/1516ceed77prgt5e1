package controlador;

import java.util.HashSet;
import java.util.Iterator;
import modelo.Grupo;
import modelo.IModelo;
import vista.IVista;
import vista.Vista;
import vista.VistaGrupo;

/**
 * Fichero: ControladorGrupo.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class ControladorGrupo {
    private IModelo modelo;
    
    public ControladorGrupo (IModelo m) {
        modelo = m;
        Boolean bucle = true;
        do {
            char opcion = Vista.menuCrud();
            switch (opcion) {
                case 'e':
                    return;
                case 'c':
                    create();
                    break;
                case 'r':
                    read();
                    break;
                case 'u':
                    update();
                    break;
                case 'd':
                    delete();
                    break;
                default:
                    Vista.mostrarTextoError("\n¡Error! Debes escoger una de las opciones del menú.");
                    break;
            }
        } while (bucle); 
    }
    
    private void create() {      
        IVista<Grupo> vg = new VistaGrupo();
        Grupo grupo = vg.tomaDatos();
        modelo.create(grupo);
        Vista.mostrarTexto("\nGrupo dado de alta con éxito.");
    }
    
    private void read() {
        VistaGrupo vg = new VistaGrupo();
        HashSet hashset = modelo.readGrupo();
        vg.mostrarVarios(hashset);
    }
    
    private void update() {
        Vista.mostrarTexto("\nEscribe el código del grupo que quieres modificar: ");
        String id = Vista.leerTexto();
        Grupo grupo = null;
        HashSet grupos = modelo.readGrupo();
        Iterator iterator = grupos.iterator();
        while (iterator.hasNext()) {
            Grupo g = (Grupo) iterator.next();
            if (id.equals(g.getId())) {
                grupo = g;
            }
        }
        IVista<Grupo> vg = new VistaGrupo();
        grupo = vg.tomaDatos();
        grupo.setId(id);
        modelo.update(grupo);
        Vista.mostrarTexto("\nGrupo actualizado con éxito.");
    }
    
    private void delete() {       
        Vista.mostrarTexto("\nIntroduce el código del grupo que quieres borrar: ");
        String id = Vista.leerTexto();
        Grupo grupo = null;
        HashSet grupos = modelo.readGrupo();
        Iterator iterator = grupos.iterator();
        while (iterator.hasNext()) {
            Grupo g = (Grupo) iterator.next();
            if (id.equals(g.getId())) {
                grupo = g;
            }
        }
        modelo.delete(grupo);
        Vista.mostrarTexto("\nGrupo borrado con éxito.");
    }   
}