package controlador;

import vista.Vista;

/**
 * Fichero: Configurador.java
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/01/2016
 */

public class Configurador {
    private static Configurador instancia;
    private static String nombre;

    public static String getNombreApp() {
        return nombre;
    }

    private Configurador(String nombre) {
        this.nombre = nombre;
    }

    public static Configurador getInstanciaApp(String nombre) {
        if (instancia == null) {
            instancia = new Configurador(nombre);
            Configurador.nombre = nombre;
            Vista.mostrarTexto("\nCreada instancia Configurador.");
        } else {
            Vista.mostrarTextoError("\nError : Solo se permite una instancia.");
        }
        return instancia;
    }
}
