package controlador;

import java.util.InputMismatchException;
import modelo.IModelo;
import vista.Vista;

/**
 * Fichero: Main.java
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/01/2016
 */

/*
* Clase principal
* Controla el programa
* Contiene la función principal
* Importa las clases de los paquetes modelo y vista
*/

public class Main {
    public static void main(String[]args) throws InputMismatchException {
        IModelo modelo = null;
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}