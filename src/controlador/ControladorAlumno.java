package controlador;

import java.util.HashSet;
import java.util.Iterator;
import modelo.Alumno;
import modelo.Grupo;
import modelo.IModelo;
import vista.IVista;
import vista.Vista;
import vista.VistaAlumno;

/**
 * Fichero: ControladorAlumno.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class ControladorAlumno {
    private IModelo modelo;
    private Vista vista;
    
    public ControladorAlumno (IModelo m) {
        modelo = m;
        Boolean bucle = true;
        do {
            char opcion = vista.menuCrud();
            switch (opcion) {
                case 'e':
                    return;
                case 'c':
                    create();
                    break;
                case 'r':
                    read();
                    break;
                case 'u':
                    update();
                    break;
                case 'd':
                    delete();
                    break;
                default:
                    Vista.mostrarTextoError("\n¡Error! Debes escoger una de las opciones del menú.");
                    break;
            }
        } while (bucle); 
    }
    
    private void create() {      
        IVista<Alumno> va = new VistaAlumno();
        Alumno alumno = va.tomaDatos();
        Grupo grupo = getGrupo();
        if (grupo != null) {
            alumno.setGrupo(grupo);
            modelo.create(alumno);
            Vista.mostrarTexto("\nAlumno dado de alta con éxito.");
        } else {
            Vista.mostrarTextoError("\n¡Error! El grupo seleccionado no existe.");
        }
    }
    
    private void read() {
        VistaAlumno va = new VistaAlumno();
        HashSet hashset = modelo.readAlumno();
        va.mostrarVarios(hashset);
    }
    
    private void update() {
        Vista.mostrarTexto("\nEscribe el código del alumno que quieres modificar: ");
        String id = Vista.leerTexto();
        Alumno alumno = null;
        HashSet alumnos = modelo.readAlumno();
        Iterator iterator = alumnos.iterator();
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (id.equals(a.getId())) {
                alumno = a;
            }
        }
        IVista<Alumno> vg = new VistaAlumno();
        alumno = vg.tomaDatos();
        alumno.setId(id);
        Grupo grupo = getGrupo();
        if (grupo != null) {
            alumno.setGrupo(grupo);
            modelo.update(alumno);
            Vista.mostrarTexto("\nAlumno actualizado con éxito.");
        } else {
            Vista.mostrarTextoError("\n¡Error! El grupo seleccionado no existe.");
        }
        
    }
    
    private void delete() {       
        Vista.mostrarTexto("\nIntroduce el código del alumno que quieres borrar: ");
        String id = Vista.leerTexto();
        Alumno alumno = null;
        HashSet alumnos = modelo.readAlumno();
        Iterator iterator = alumnos.iterator();
        while (iterator.hasNext()) {
            Alumno a = (Alumno) iterator.next();
            if (id.equals(a.getId())) {
                alumno = a;
            }
        }
        modelo.delete(alumno);
        Vista.mostrarTexto("\nAlumno borrado con éxito.");
    }   
    
    private Grupo getGrupo() {
       /*
        * Este método sirve para obtener un grupo a partir de un vector o 
        * colección de cursos. Si el ID no existe, el método devuelve un objeto
        * curso NULL. Lo utilizan las funciones create() y update()
        */
        Grupo grupo = null;                           //Creamos un objeto grupo NULL
        Vista.mostrarTexto("\nIntroduce el código del nuevo grupo: ");
        String idC = Vista.leerTexto();         //Recogemos el ID de grupo que nos da el usuario
        HashSet cursos = modelo.readGrupo();         //Creamos un hashset de grupo
        Iterator iterator = cursos.iterator();       //Creamos un objeto iterator para el hashset
        while (iterator.hasNext()) {                 //Iteramos el hashset
            Grupo g = (Grupo) iterator.next();
            if (idC.equals(g.getId())) {       //Si el ID proporcionado existe, nos guardamos el grupo en el objeto grupo
                grupo = g;
            }
        }
        return grupo;                               //Devolvemos el objeto grupo. Será NULL si el ID no existía
    }
}