package controlador;

import modelo.Alumno;
import modelo.Grupo;
import modelo.IModelo;
import modelo.ModeloVector;
import modelo.ModeloHashSet;
import vista.Vista;
import vista.IVista;
import vista.VistaAlumno;
import vista.VistaGrupo;
import java.util.InputMismatchException;

/**
 * Fichero: Controlador.java
 * 
 * @author Raquel Pont Lillo <raquel.pont.lillo@gmail.com>
 * @date fecha: 10/11/2016
 */

public class Controlador {
    private Alumno alumno = new Alumno();
    private Grupo grupo = new Grupo();
    private Vista vista = new Vista();
    private IVista<Grupo> vg = new VistaGrupo();
    private IVista<Alumno> va = new VistaAlumno();
    private IModelo modelo = null;
    
    public Controlador(IModelo m, Vista v) throws InputMismatchException {
        Configurador.getInstanciaApp("\nPRÁCTICAS CLASE");
        Configurador.getInstanciaApp("** PRÁCTICAS DE CLASE **");
        
        modelo = m;
        vista = v;
        Boolean bucle = true;
        
        do {
            try {
                int opcion = vista.menuEstructura();
                switch (opcion) {
                    case 'e':
                        return;
                    case 'a':
                        modelo = new ModeloVector();
                        bucle = false;
                        break;
                    case 'h':
                        modelo = new ModeloHashSet();
                        bucle = false;
                        break;
                } 
            } catch (InputMismatchException e) {
                Vista.error();
            }   
        } while (bucle);
        
        bucle = true;
        
        do {
            try {
                int opcion = vista.menu();
                switch (opcion) {
                    case 0:
                        return;
                    case 1:
                        ControladorGrupo cg = new ControladorGrupo(modelo);
                        break;
                    case 2:
                        ControladorAlumno ca = new ControladorAlumno (modelo);
                        break;
                } 
            } catch (InputMismatchException e) {
                Vista.error();
            }   
        } while (bucle);
    }
}
